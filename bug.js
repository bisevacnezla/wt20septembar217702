const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes){
    const Bug = sequelize.define('Bug', {
        opis: Sequelize.STRING,
        projekat: Sequelize.STRING
    })
    return Bug
} 
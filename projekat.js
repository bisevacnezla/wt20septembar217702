const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes){
    const Projekat = sequelize.define('Projekat', {
        naziv: Sequelize.STRING,
        link:Sequelize.STRING
    })
    return Projekat
} 
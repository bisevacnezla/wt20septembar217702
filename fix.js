const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes){
    const Fix = sequelize.define('Fix', {
        opis: Sequelize.STRING,
        hash: Sequelize.STRING,
        datumVrijeme: Sequelize.DATE
    })
    return Fix
} 
const express = require('express')
const bodyParser = require('body-parser')
const db=require('./db.js')
const app = express()

app.use(bodyParser.json)
app.use(bodyParser.urlencoded({extended:true}))
app.use(express.static(__dirname))


db.sequelize.sync({force:true}).then(function(){
    inicijalizacija().then(function(){
        console.log('Gotovo kreiranje i popunjavanje tabela')
    })
})

function inicijalizacija(){
    var komentarListaPromisea = []
    var projekatListaPromisea = []
    var bugListaPromisea = []
    var fixListaPromisea = []

    return new Promise(function(resolve, reject){


        projekatListaPromisea.push(db.Projekat.create({id: 1, naziv: 'Naziv', link: 'www.projekat.com', bug: 1}))
        projekatListaPromisea.push(db.Projekat.create({id: 2, naziv: 'projekat2', link: null, bug: 2}))
        projekatListaPromisea.push(db.Projekat.create({id: 2, naziv: 'projekat3', link: null, bug: 3}))

        fixListaPromisea.push(db.Fix.create({id: 1, opis: 'opis', hash: null, datumVrijeme: 2020-05-05}))
        fixListaPromisea.push(db.Fix.create({id: 2, opis: 'opis2', hash: null, datumVrijeme: 2019-05-03}))
        fixListaPromisea.push(db.Fix.create({id: 3, opis: 'opis3', hash: null, datumVrijeme: 2019-05-03}))

        bugListaPromisea.push(db.Bug.create({id: 1, opis: 'opis', projekat: 'Naziv', komentar: null, fix: 1}))
        bugListaPromisea.push(db.Bug.create({id: 2, opis: 'opis2', projekat: 'projekat2', komentar: null, fix: 2}))
        bugListaPromisea.push(db.Bug.create({id: 3, opis: 'opis3', projekat: 'projekat3', komentar: null, fix: 3}))


    })
}

/**kreira bug sa svim proslijeđenim parametrima i dodjeljuje ga nekom projektu, u slučaju da postoji bug 
 * sa istim opisom vratite grešku da je bug dupli*/
app.post('/bug', function(req, res){
    var id = req.body['id']
    var opis = req.body['opis']
    var projekat = req.body['projekat']
    var komentari = req.body['komentar']
    var fix = req.body['fix']
    db.Bug.findAll({where: {opis: opis}}).then(function(bug){
        if(bug==null){
            db.Bug.create({id: id, opis: opis, projekat: projekat, komentar: komentari, fix: fix}).then(function(){
                console.log('Bug kreiran!')
            })

            db.projekat.findOne({where: {naziv: bug.projekat}}).then(function(projekat){
                projekat.addBug({where: {id: id}})
            })
        }
        else{
            res.send({message: 'Opis buga je dupli!'})
        }
    }).catch(function(err){
        console.log(err)
    })
})

/**GET /projekat/:id/openbugs - vraća listu svih bugova koji nemaju fix, a pridruženi su projektu sa id-em koji je proslijeđen u url-u */
app.get('/projekat/:id/openbugs', function(req, res){
    var id = req.params['id']
    db.Projekat.findAll({where: {bug: id}}).then(function(bug){
        db.Bug.findAll({where: {fix: null, id: bug.id}}).then(function(nadjeno){
            nadjeno.foreach(bug2=> console.log(bug.opis + '\n'))
        })
    })
})

/**GET /fix/lastProjects - vraća listu zadnjih 5 fixova i njihovih projekata, ako su dva ili više fixa na isti projekat ne vraćajte duple podatke. 
 * Odgovor neka bude u json formatu {fixes:[...],projects:[...]} */

app.get('/fix/lastProjects', function(req,res){
    var nizBug = []


    db.Bug.findAll({where: {id: {[Op.gt]: 1}}}).then(function(bug1){
        db.Projekat.findAll({where: {id: bug1.id}}).then(function(projekat){
            nizBug.push(projekat.bug)
        })
    })
    
    var nizFix = []
    for(var i=0; i<nizBug.length; i++){
        db.Fix.findAll({where: {id: nizBug[i].fix}}).then(function(fix){
            nizFix.push(fix)
        })
    }
    var b=0;
    for(var i=nizFix.length()-1; i>=0; i--){
        res.send({message: 'fixes: ['+nizFix[i] + '], projects: ['+nizBug[i].projekat+']'+'\n'})
    }
})

app.listen(8085, ()=>{
    console.log('Server je na portu 8085')
})
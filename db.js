const Sequelize = require('sequelize')
const sequelize = new Sequelize('popravni2', 'root', '', {
    logging:false,
    host: '127.0.0.1',
    dialect: 'mysql',
    define:{
        timestamps: false,
        freezeTableName: true
    }
})


const db={}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.Projekat = sequelize.import(__dirname + '/projekat.js')
db.Komentar = sequelize.import(__dirname + '/komentar.js')
db.Fix = sequelize.import(__dirname + '/fix.js')
db.Bug = sequelize.import(__dirname + '/bug.js')

db.Bug.hasMany(db.Projekat, {foreignKey: 'bug'})

db.Komentar.hasMany(db.Bug, {foreignKey: 'komentar'})
db.Komentar.belongsTo(db.Komentar, {foreignKey: 'roditeljski'})
db.Fix.hasOne(db.Bug, {foreignKey: 'fix'})

module.exports=db